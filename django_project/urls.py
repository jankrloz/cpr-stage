from django.conf.urls import include, url, patterns
from django.contrib import admin

urlpatterns = [
    url(r'^', include('apps.main.urls', namespace='main_app')),
    url(r'^', include('apps.miembros.urls', namespace='miembros_app')),
    url(r'^eventos/', include('apps.eventos.urls', namespace='eventos_app')),
    url(r'^difusion/', include('apps.difusion.urls', namespace='difusion_app')),
    url(r'^preregistros/', include('apps.preregistros.urls', namespace='preregistros_app')),
    url(r'^CIR/', include('apps.CIR.urls', namespace='cir_app')),

    url(r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
    url(r'^redactor/', include('redactor.urls')),
    url(r'^admin/', include(admin.site.urls)),
]

from django.conf import settings

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
                            (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT}))
