# -*- encoding: utf-8 -*-
from django import forms
from .models import *


class LoginForm(forms.Form):
    matricula = forms.CharField(max_length=20,
                               widget=forms.TextInput(attrs={
                                   'placeholder': 'Matricula',
                                   'class': 'form-control',
                               }))
    password = forms.CharField(max_length=50,
                               widget=forms.TextInput(attrs={
                                   'type': 'password',
                                   'placeholder': 'Contraseña',
                                   'class': 'form-control',
                               }))

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data


class SignUpForm(forms.ModelForm):
    class Meta:
        model = Miembro
        fields = ('matricula', 'nombre', 'email', 'password')

        widgets = {
            'matricula': forms.TextInput(attrs={
                'placeholder': 'Matrícula',
                'class': 'form-control',
            }),
            'nombre': forms.TextInput(attrs={
                'placeholder': 'Nombre',
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email',
                'type': 'email',
                'class': 'form-control',
            }),
            'password': forms.TextInput(attrs={
                'type': 'password',
                'placeholder': 'Crear contraseña',
                'class': 'form-control psw',
            }),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data



class EditarMiembroForm(forms.ModelForm):
    class Meta:
        model = Miembro
        fields = ('matricula', 'nombre', 'email', 'avatar')

        widgets = {
            'matricula': forms.TextInput(attrs={
                'placeholder': 'Matrícula',
                'class': 'form-control',
            }),
            'nombre': forms.TextInput(attrs={
                'placeholder': 'Nombre',
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email',
                'type': 'email',
                'class': 'form-control',
            }),
            'avatar': forms.FileInput(),
        }
