# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils.datetime_safe import *


class UserManager(BaseUserManager, models.Manager):
    def _create_user(self, matricula, nombre, password, is_staff,
                     is_superuser, **extra_fields):

        user = self.model(matricula=matricula, nombre=nombre, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, matricula, nombre, password=None, **extra_fields):
        return self._create_user(matricula, nombre, password, False,
                                 False, **extra_fields)

    def create_superuser(self, matricula, nombre, password, **extra_fields):
        return self._create_user(matricula, nombre, password, True,
                                 True, **extra_fields)


class Miembro(AbstractBaseUser, PermissionsMixin):
    matricula = models.CharField(max_length=20, unique=True)
    nombre = models.CharField(max_length=500)
    fecha_registro = models.DateField(default=datetime.now)
    email = models.EmailField(blank=True)

    # roles = (
    #     ('DA', 'director_administracion'),
    #     ('DD', 'director_difusion'),
    #     ('C', 'coordinador'),
    #     ('M', 'miembro'),
    # )
    # rol = models.CharField(max_length=2, choices=roles, default='M')

    avatar = models.ImageField(upload_to='usuarios/avatars', blank=True)

    # Los siguientes 2 campos son requeridos para un modelo de usuarios
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'matricula'
    REQUIRED_FIELDS = ['nombre']

    class Meta:
        verbose_name = 'Miembro'
        verbose_name_plural = 'Miembros'

    @property
    def get_short_name(self):
        return self.nombre

    @property
    def get_full_name(self):
        return self.matricula

    @property
    def get_image_url(self):
        return self.avatar.url

    def __str__(self):
        return '%s - %s' % (self.matricula, self.nombre)


class DirectorAdmin(models.Model):
    miembro = models.OneToOneField(Miembro)

    class Meta:
        verbose_name = 'Director Administrativo'
        verbose_name_plural = 'Directores Administrativos'

    def __str__(self):
        return self.miembro.matricula


class DirectorDifusion(models.Model):
    miembro = models.OneToOneField(Miembro)

    class Meta:
        verbose_name = 'Director de Difusión'
        verbose_name_plural = 'Directores de Difusión'

    def __str__(self):
        return self.miembro.matricula


class CoordinadorRegistro(models.Model):
    miembro = models.OneToOneField(Miembro)

    class Meta:
        verbose_name = 'Coordinador de Registro'
        verbose_name_plural = 'Coordinadores de Registro'

    def __str__(self):
        return self.miembro.matricula
