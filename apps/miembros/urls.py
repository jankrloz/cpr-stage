from django.conf.urls import url


urlpatterns = [
    url(r'^login/$', 'apps.miembros.views.login_miembro', name='login'),
    url(r'^logout/$', 'apps.miembros.views.logout_miembro', name='logout'),

    url(r'^gestion_miembros/$', 'apps.miembros.views.gestion_miembros', name='gestion_miembros'),
    url(r'^editar_miembro/(?P<id_miembro>\d+)/$', 'apps.miembros.views.editar_miembro', name='editar_miembro'),
]
