# -*- encoding: utf-8 -*-
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render_to_response
from django.views.generic import FormView
from .models import *
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from .forms import *


def login_miembro(request):
    login_form = LoginForm()
    signup_form = SignUpForm()

    if request.method == 'POST':
        if 'signup_form' in request.POST:
            signup_form = SignUpForm(request.POST)
            if signup_form.is_valid():
                Miembro.objects.create_user(matricula=signup_form.cleaned_data['matricula'],
                                            nombre=signup_form.cleaned_data['nombre'],
                                            email=signup_form.cleaned_data['email'],
                                            password=signup_form.cleaned_data['password'])

                user = authenticate(matricula=signup_form.cleaned_data['matricula'],
                                    password=signup_form.cleaned_data['password'])

                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('/')

        if 'login_form' in request.POST:
            login_form = LoginForm(request.POST)
            if login_form.is_valid():
                user = authenticate(matricula=login_form.cleaned_data['matricula'],
                                    password=login_form.cleaned_data['password'])
                if user is not None:
                    if user.is_active:
                        print(user)
                        login(request, user)
                        return redirect('/')
                else:
                    print("el usuario y contraseña son invalidos")
                    return render(request, 'miembros/login.html', {'login_form': login_form,
                                                                   'Error': "El username o password no son validos"})

    return render(request, 'miembros/login.html', {'login_form': login_form,
                                                   'signup_form': signup_form})


def logout_miembro(request):
    logout(request)
    return redirect('/login')


def gestion_miembros(request):
    lista_miembros = Miembro.objects.all().order_by('-fecha_registro')
    return render(request, 'miembros/gestion_miembros.html', {'miembros': lista_miembros})


def editar_miembro(request, id_miembro):
    miembro = Miembro.objects.get(id=id_miembro)
    miembro_form = EditarMiembroForm(instance=miembro)
    if request.method == 'POST':
        if 'miembro_form' in request.POST:
            miembro_form = EditarMiembroForm(request.POST, request.FILES, instance=miembro)
            if 'new_password' in request.POST:
                miembro.set_password(request.POST['new_password'])
            if miembro_form.is_valid():
                miembro_form.save()
                return redirect(reverse_lazy('miembros_app:gestion_miembros'))
        elif 'borrar_miembro' in request.POST:
            miembro.delete()
            return redirect(reverse_lazy('miembros_app:gestion_miembros'))

    return render(request, 'miembros/editar_miembro.html', {'miembro': miembro,
                                                            'miembro_form': miembro_form})


def crear_matricula(request):
    nueva_matricula = Matricula.objects.create()
