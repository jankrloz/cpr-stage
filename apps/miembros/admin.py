from django.contrib import admin
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from .models import *

admin.site.register(ContentType)
admin.site.register(Permission)

admin.site.register(Miembro)
admin.site.register(DirectorAdmin)
admin.site.register(DirectorDifusion)
admin.site.register(CoordinadorRegistro)
