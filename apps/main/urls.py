from django.conf.urls import url


urlpatterns = [
    url(r'^$', 'apps.main.views.index', name='index'),
]
