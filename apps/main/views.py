from django.shortcuts import render
from apps.difusion.models import Noticia

def index(request):
    noticias = Noticia.objects.all().order_by('-fecha_creacion')
    return render(request, 'index.html', {'noticias': noticias})
