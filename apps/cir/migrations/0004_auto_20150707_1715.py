# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):
    dependencies = [
        ('CIR', '0003_auto_20150706_1843'),
    ]

    operations = [
        migrations.AddField(
            model_name='competencia',
            name='categorias',
            field=models.ManyToManyField(to='CIR.Categoria'),
        ),
        migrations.AlterField(
            model_name='competencia',
            name='fecha_inscripciones',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 7, 17, 15, 37, 372921)),
        ),
    ]
