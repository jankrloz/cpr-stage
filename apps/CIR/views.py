from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render_to_response
from .forms import *


def inscripcion_competencia(request):
    inscripcion_form = InscrForm()
    if request.method == 'POST':
        inscripcion_form = InscrForm(request.POST)
        if 'inscripcion_form' in request.POST:
            if inscripcion_form.is_valid():
                req = request.POST
                competencia = Competencia.objects.get(pk=int(req['competencia']))
                categoria = Categoria.objects.get(pk=int(req['categoria']))
                equipo = Equipo.objects.create(nombre=req['nombre_equipo'],
                                               universidad=req['universidad'],
                                               asesor=req['asesor'],
                                               nombre_prototipo=req['nombre_prototipo'])

                for (nombre, email) in zip(req.getlist('nombre_integrante'), req.getlist('email_integrante')):
                    Participante.objects.create(nombre=nombre,
                                                email=email,
                                                equipo=equipo)

                Inscripcion.objects.create(competencia=competencia,
                                           categoria=categoria,
                                           equipo=equipo)

                return redirect(reverse('main_app:index'))

    return render(request, 'CIR/inscripcion_equipo.html', {'inscripcion_form': inscripcion_form})


def ver_inscripciones(request):
    inscripciones = Inscripcion.objects.filter(status=False)
    return render(request, 'CIR/ver_inscripciones.html', {'inscripciones': inscripciones})


def detalle_inscripcion(request, id_inscripcion):
    inscripcion = get_object_or_404(Inscripcion, pk=id_inscripcion)
    participantes = inscripcion.equipo.participante_set.all()
    if request.method == 'POST':
        if 'inscribir_equipo' in request.POST:
            inscripcion.status = True
            inscripcion.save()
            return redirect(reverse('cir_app:ver_inscripciones'))
    return render(request, 'CIR/detalle_inscripcion.html', {'inscripcion': inscripcion,
                                                            'participantes': participantes})
