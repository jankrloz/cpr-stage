from django.conf.urls import url

urlpatterns = [
    url(r'^inscribirse/$', 'apps.CIR.views.inscripcion_competencia', name='inscribirse'),
    url(r'^inscripciones/$', 'apps.CIR.views.ver_inscripciones', name='ver_inscripciones'),
    url(r'^inscripciones/(?P<id_inscripcion>\d+)/$', 'apps.CIR.views.detalle_inscripcion', name='detalle_inscripcion'),
]
