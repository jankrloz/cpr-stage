from django import forms
from .models import *

queryset = Competencia.objects.all()
c_ids = [c.id for c in queryset if c.inscripcion_activa()]
competencias_activas = queryset.filter(id__in=c_ids)

class InscripcionForm(forms.ModelForm):
    class Meta:
        model = Inscripcion
        fields = ('competencia', 'categoria', )


class InscrForm(forms.Form):
    competencia = forms.ModelChoiceField(queryset=competencias_activas,
                                         widget=forms.Select(attrs={'class': 'form-control'}))
    categoria = forms.ModelChoiceField(queryset=Categoria.objects.all(),
                                       widget=forms.Select(attrs={'class': 'form-control'}))

    nombre_equipo = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))

    universidad = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))

    asesor = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))

    nombre_prototipo = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
    }))
