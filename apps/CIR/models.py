from django.db import models
from datetime import datetime
from django.utils import timezone

class Categoria(models.Model):
    nombre = models.CharField(max_length=500)

    def __str__(self):
        return self.nombre


class Competencia(models.Model):
    titulo = models.CharField(max_length=500)
    sede = models.CharField(max_length=500)
    descripcion = models.TextField()
    fecha_inscripciones = models.DateTimeField(default=datetime.now())
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    categorias = models.ManyToManyField(Categoria)

    def inscripcion_activa(self):
        if self.fecha_inscripciones <= timezone.now() <= self.fecha_inicio:
            return True

    @property
    def competencia_activa(self):
        if self.fecha_inicio <= datetime.now() <= self.fecha_fin:
            return True
        else:
            return False

    def __str__(self):
        return self.titulo


class Equipo(models.Model):
    nombre = models.CharField(max_length=500)
    universidad = models.CharField(max_length=500)
    asesor = models.CharField(max_length=500, blank=True)
    nombre_prototipo = models.CharField(max_length=500)

    def __str__(self):
        return self.nombre


class Participante(models.Model):
    nombre = models.CharField(max_length=500)
    email = models.EmailField()
    equipo = models.ForeignKey(Equipo)

    def __str__(self):
        return self.nombre


class Inscripcion(models.Model):
    competencia = models.ForeignKey(Competencia)
    categoria = models.ForeignKey(Categoria)
    equipo = models.ForeignKey(Equipo)

    status = models.BooleanField(default=False)

    def __str__(self):
        return '%s - %s - %s' % (self.equipo.nombre, self.competencia.titulo, self.categoria.nombre)
