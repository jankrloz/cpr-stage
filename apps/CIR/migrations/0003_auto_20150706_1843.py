# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):
    dependencies = [
        ('CIR', '0002_competencia_fecha_inscripciones'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipo',
            name='titulo',
        ),
        migrations.AlterField(
            model_name='competencia',
            name='fecha_inscripciones',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 6, 18, 43, 25, 86186)),
        ),
    ]
