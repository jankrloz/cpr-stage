# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):
    dependencies = [
        ('CIR', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='competencia',
            name='fecha_inscripciones',
            field=models.DateTimeField(default=datetime.datetime(2015, 7, 6, 17, 35, 42, 834237)),
        ),
    ]
