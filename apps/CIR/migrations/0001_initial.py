# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nombre', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Competencia',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('titulo', models.CharField(max_length=500)),
                ('sede', models.CharField(max_length=500)),
                ('descripcion', models.TextField()),
                ('fecha_inicio', models.DateTimeField()),
                ('fecha_fin', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nombre', models.CharField(max_length=500)),
                ('universidad', models.CharField(max_length=500)),
                ('asesor', models.CharField(max_length=500, blank=True)),
                ('titulo', models.CharField(max_length=500)),
                ('nombre_prototipo', models.CharField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='Inscripcion',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('status', models.BooleanField(default=False)),
                ('categoria', models.ForeignKey(to='CIR.Categoria')),
                ('competencia', models.ForeignKey(to='CIR.Competencia')),
                ('equipo', models.ForeignKey(to='CIR.Equipo')),
            ],
        ),
        migrations.CreateModel(
            name='Participante',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('nombre', models.CharField(max_length=500)),
                ('email', models.EmailField(max_length=254)),
                ('equipo', models.ForeignKey(to='CIR.Equipo')),
            ],
        ),
    ]
