from django.contrib import admin
from .models import *

admin.site.register(Competencia)
admin.site.register(Categoria)
admin.site.register(Equipo)
admin.site.register(Participante)
admin.site.register(Inscripcion)
