# -*- encoding: utf-8 -*-
from django import forms
from .models import *
from datetimewidget.widgets import DateTimeWidget


class EventoForm(forms.ModelForm):
    class Meta:
        model = EventosAdmin
        fields = ('titulo', 'descripcion', 'fecha')

        widgets = {
            'titulo': forms.TextInput(attrs={
                'placeholder': 'Titulo',
                'class': 'form-control',
            }),
            'descripcion': forms.TextInput(attrs={
                'placeholder': 'Descripcion',
                'class': 'form-control',
            }),
            'fecha': DateTimeWidget(attrs={'id': "yourdatetimeid"}, usel10n=True, bootstrap_version=3)
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data
