# -*- encoding: utf-8 -*-
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, redirect
from .forms import *
from .models import *


def eventos_admin(request):
    eventos_admin = EventosAdmin.objects.all()
    return render(request, 'eventos/eventos_admin.html',
                  {'eventos_admin': eventos_admin})


def crear_evento_admin(request):
    evento_form = EventoForm()
    if request.method == 'POST':
        evento_form = EventoForm(request.POST)
        if 'evento_form' in request.POST:
            if evento_form.is_valid():
                evento_form.save()
                return redirect(reverse('eventos_app:eventos_admin'))

    return render(request, 'eventos/crear_evento_admin.html',
                  {'evento_form': evento_form})


def editar_evento_admin(request, id_evento):
    evento = EventosAdmin.objects.get(id=id_evento)
    evento_form = EventoForm(instance=evento)
    if request.method == 'POST':
        if 'evento_form' in request.POST:
            evento_form = EventoForm(request.POST, instance=evento)
            if evento_form.is_valid():
                evento_form.save()
                return redirect(reverse_lazy('eventos_app:eventos_admin'))
        elif 'borrar_evento' in request.POST:
            evento.delete()
            return redirect(reverse_lazy('eventos_app:eventos_admin'))

    return render(request, 'eventos/editar_evento_admin.html', {'evento': evento,
                                                                'evento_form': evento_form})
