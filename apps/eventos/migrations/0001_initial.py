# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('miembros', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='CalendarioAdmin',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name_plural': 'Calendarios Administrativos',
                'verbose_name': 'Calendario Administrativo',
            },
        ),
        migrations.CreateModel(
            name='EventosAdmin',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
                ('descripcion', models.TextField()),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('fecha', models.DateTimeField()),
                ('autor', models.OneToOneField(to='miembros.DirectorAdmin')),
            ],
            options={
                'verbose_name_plural': 'Eventos Administrativos',
                'verbose_name': 'Evento Administrativo',
            },
        ),
        migrations.AddField(
            model_name='calendarioadmin',
            name='eventos',
            field=models.ForeignKey(to='eventos.EventosAdmin'),
        ),
    ]
