from django.db import models
from apps.miembros.models import *


class EventosAdmin(models.Model):
    titulo = models.CharField(max_length=200)
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha = models.DateTimeField()

    class Meta:
        verbose_name = 'Evento Administrativo'
        verbose_name_plural = 'Eventos Administrativos'

    def __str__(self):
        return self.titulo
