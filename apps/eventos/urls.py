from django.conf.urls import url

urlpatterns = [
    url(r'^admin/$', 'apps.eventos.views.eventos_admin', name='eventos_admin'),
    url(r'^admin/crear/$', 'apps.eventos.views.crear_evento_admin', name='crear_evento_admin'),
    url(r'^admin/editar/(?P<id_evento>\d+)/$', 'apps.eventos.views.editar_evento_admin', name='editar_evento_admin'),
    url(r'^logout/$', 'apps.miembros.views.logout_miembro', name='logout'),

    url(r'^gestion_miembros/$', 'apps.miembros.views.gestion_miembros', name='gestion_miembros'),
    url(r'^editar_miembro/(?P<id_miembro>\d+)/$', 'apps.miembros.views.editar_miembro', name='editar_miembro'),
]
