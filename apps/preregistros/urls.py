# -*- encoding: utf-8 -*-
from django.conf.urls import url
from .views import *

urlpatterns = [

    url(r'generar_matricula/(?P<id_preregistro>\d+)/$', 'apps.preregistros.views.generar_matricula',
        name='generar_matricula'),

    url(r'success/$', 'apps.preregistros.views.preregistro_success', name='preregistro_success'),

    url(r'crear/$', 'apps.preregistros.views.crear_preregistro', name='crear'),
    url(r'comprobantes_actividad/(?P<id_preregistro>\d+)/$', ComprobantesActividadView.as_view(),
        name='comprobantes_actividad'),

    url(r'(?P<id_preregistro>\d+)/$', 'apps.preregistros.views.detalle_preregistro', name='detalle_preregistro'),

    url(r'$', 'apps.preregistros.views.ver_preregistros', name='preregistros'),
]
