# -*- encoding: utf-8 -*-
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.shortcuts import render_to_response
from django.views.generic import FormView
import datetime

from .forms import *
from apps.miembros.models import Miembro

# Create your views here.

def crear_preregistro(request):
    preregistro_form = PreregistroForm()

    if request.method == 'POST':
        print(request.POST)
        if 'preregistro_form' in request.POST:
            preregistro_form = PreregistroForm(request.POST, request.FILES)
            if preregistro_form.is_valid():
                nuevo_preregistro = preregistro_form.save()
                return redirect(reverse_lazy('preregistros_app:comprobantes_actividad',
                                             kwargs={'id_preregistro': nuevo_preregistro.id}))
            else:
                print('registro incompleto')

    return render(request, 'preregistros/preregistro.html', {'preregistro_form': preregistro_form})

@permission_required('preregistros.add_preregistro')
def ver_preregistros(request):
    preregistros = Preregistro.objects.all().order_by('-fecha_preregistro')
    return render(request, 'preregistros/ver_preregistros.html', {'preregistros': preregistros})


class ComprobantesActividadView(FormView):
    template_name = 'preregistros/comprobantes_actividad.html'
    form_class = ComprobanteActividadForm
    success_url = '/preregistros/success/'

    def form_valid(self, form):
        id_preregistro = int(self.kwargs['id_preregistro'])
        print(id_preregistro)
        preregistro = Preregistro.objects.get(id=id_preregistro)

        for each in form.cleaned_data['comprobante_actividad']:
            ComprobanteActividad.objects.create(preregistro=preregistro,
                                                comprobante_actividad=each)

        return super(ComprobantesActividadView, self).form_valid(form)


@permission_required('preregistros.add_preregistro')
def detalle_preregistro(request, id_preregistro):
    preregistro = Preregistro.objects.get(id=id_preregistro)
    comprobantes_actividad = ComprobanteActividad.objects.filter(preregistro=preregistro)

    return render_to_response('preregistros/detalle_preregistro.html',
                              {'preregistro': preregistro,
                               'comprobantes_actividad': comprobantes_actividad})



def preregistro_success(request):
    return render_to_response('preregistros/preregistro_success.html')


def generar_matricula(request, id_preregistro):
    preregistro = get_object_or_404(Preregistro, pk=id_preregistro)

    now = datetime.now()

    nuevo_miembro, created = Miembro.objects.get_or_create(email=preregistro.email, nombre=preregistro.nombre)
    nueva_matricula = 'CPR{0}{1}'.format(now.year, str(nuevo_miembro.id).zfill(4))
    nuevo_miembro.matricula = nueva_matricula
    nuevo_miembro.email = preregistro.email
    nuevo_miembro.nombre = preregistro.nombre
    nuevo_miembro.set_password(nueva_matricula)
    nuevo_miembro.save()

    preregistro.delete()

    from django.core.mail import send_mail
    send_mail("Asunto",
          "Felicidades %s, tu nueva cuenta es:\nMatricula: %s\nPassword: %s" % (nuevo_miembro.nombre, nueva_matricula, nueva_matricula),
          'cpr.stage1@gmail.com',
          [nuevo_miembro.email])

    return redirect(reverse('preregistros_app:preregistros'))

