# -*- encoding: utf-8 -*-
from django import forms
from multiupload.fields import MultiFileField
from .models import *


class PreregistroForm(forms.ModelForm):
    class Meta:
        model = Preregistro
        fields = ('nombre', 'email', 'telefono', 'escuela', 'comprobante_estudios')

        widgets = {
            'nombre': forms.TextInput(attrs={
                'placeholder': 'Nombre Completo',
                'class': 'form-control',
            }),
            'email': forms.TextInput(attrs={
                'placeholder': 'Email',
                'class': 'form-control',
            }),
            'telefono': forms.TextInput(attrs={
                'placeholder': 'Telefono de Contacto',
                'class': 'form-control',
            }),
            'escuela': forms.TextInput(attrs={
                'placeholder': 'Escuela Superior',
                'class': 'form-control',
            }),
            'comprobante_estudios': forms.FileInput()
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data


class ComprobanteActividadForm(forms.Form):
    comprobante_actividad = MultiFileField(min_num=1, max_num=5, max_file_size=1024 * 1024 * 10)
