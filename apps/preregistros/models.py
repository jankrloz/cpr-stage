# -*- encoding: utf-8 -*-
from django.db import models
from django.utils.datetime_safe import *


class Preregistro(models.Model):
    nombre = models.CharField(max_length=500)
    email = models.EmailField()
    telefono = models.CharField(max_length=13)
    escuela = models.CharField(max_length=100)
    comprobante_estudios = models.FileField(upload_to='preregistros/comprobantes_estudios')
    fecha_preregistro = models.DateField(default=datetime.now)

    class Meta:
        verbose_name = 'Pre Registro'
        verbose_name_plural = 'Pre Registros'

    def __str__(self):
        return self.nombre


class ComprobanteActividad(models.Model):
    preregistro = models.ForeignKey(Preregistro)
    comprobante_actividad = models.FileField(upload_to='preregistros/comprobantes_actividad')

    class Meta:
        verbose_name = 'Comprobante de Actividad'
        verbose_name_plural = 'Comprobantes de Actividad'

    def __str__(self):
        return '%s - %s' % (self.preregistro, self.comprobante_actividad)
