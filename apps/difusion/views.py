from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404, redirect
from django.shortcuts import render_to_response
from .models import *
from .forms import NoticiaForm


def ver_noticias(request):
    noticias = Noticia.objects.all().order_by('-fecha_creacion')
    return render(request, 'difusion/ver_noticias.html', {'noticias': noticias})


def detalle_noticia(request, id_noticia):
    noticia = get_object_or_404(Noticia, pk=id_noticia)
    return render(request, 'difusion/detalle_noticia.html', {'noticia': noticia})


def crear_noticia(request):
    noticia_form = NoticiaForm()
    if request.method == 'POST':
        noticia_form = NoticiaForm(request.POST)
        if 'noticia_form' in request.POST:
            if noticia_form.is_valid():
                noticia_form.save()
                return redirect(reverse('difusion_app:ver_noticias'))

    return render(request, 'difusion/crear_noticia.html', {'noticia_form': noticia_form})


def editar_noticia(request, id_noticia):
    noticia = get_object_or_404(Noticia, pk=id_noticia)
    noticia_form = NoticiaForm(instance=noticia)
    if request.method == 'POST':
        noticia_form = NoticiaForm(request.POST, instance=noticia)
        if 'noticia_form' in request.POST:
            if noticia_form.is_valid():
                noticia_form.save()
                return redirect(reverse('difusion_app:ver_noticias'))
        elif 'borrar_noticia' in request.POST:
            noticia.delete()
            return redirect(reverse('difusion_app:ver_noticias'))

    return render(request, 'difusion/editar_noticia.html', {'noticia_form': noticia_form})
