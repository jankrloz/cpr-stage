from django import forms

from redactor.widgets import RedactorEditor
from .models import *


class NoticiaForm(forms.ModelForm):
    class Meta:
        model = Noticia
        fields = ('titulo', 'contenido')

        widgets = {
            'titulo': forms.TextInput(attrs={
                'type': 'text',
                'class': 'form-control',
            }),
            'contenido': RedactorEditor(attrs={
                'class': 'form-control'
            }),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        return cleaned_data
