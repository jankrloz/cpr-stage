from django.conf.urls import url

urlpatterns = [
    url(r'^noticias/$', 'apps.difusion.views.ver_noticias', name='ver_noticias'),
    url(r'^noticias/(?P<id_noticia>\d+)/$', 'apps.difusion.views.detalle_noticia', name='detalle_noticia'),
    url(r'^noticias/editar/(?P<id_noticia>\d+)/$', 'apps.difusion.views.editar_noticia', name='editar_noticia'),
    url(r'^noticias/crear/$', 'apps.difusion.views.crear_noticia', name='crear_noticia'),
]
