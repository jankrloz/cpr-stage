# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('difusion', '0002_auto_20150706_1315'),
    ]

    operations = [
        migrations.RenameField(
            model_name='noticia',
            old_name='texto',
            new_name='contenido',
        ),
    ]
